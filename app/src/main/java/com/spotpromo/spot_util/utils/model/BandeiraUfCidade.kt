package com.spotpromo.spot_util.utils.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class BandeiraUfCidade : Serializable {

    @SerializedName("codBandeira")
    var codBandeira: Int? = null
    @SerializedName("desBandeira")
    var desBandeira: String? = null
    @SerializedName("codUf")
    var codUf: Int? = null
    @SerializedName("desUf")
    var desUf: String? = null
    @SerializedName("codCidade")
    var codCidade: Int? = null
    @SerializedName("desCidade")
    var desCidade: String? = null
}