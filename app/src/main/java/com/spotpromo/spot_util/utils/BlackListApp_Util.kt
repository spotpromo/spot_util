package com.spotpromo.spot_util.utils

import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.net.TrafficStats
import android.util.Log
import com.spotpromo.spot_util.R
import com.spotpromo.spot_util.utils.databasehelper.SqliteDataBaseHelper
import com.spotpromo.spot_util.utils.databasehelper.dao.BlackListAppDAOHelper
import com.spotpromo.spot_util.utils.model.BlackListApp
import java.util.ArrayList

class BlackListApp_Util {

    companion object {
        @Throws(Exception::class)
        fun retornaListAppBlackList(context: Context): ArrayList<BlackListApp> {
            val listblapp = ArrayList<BlackListApp>()

            try {

                val pm = context.packageManager
                //get a list of installed apps.
                val packages = pm.getInstalledApplications(PackageManager.GET_META_DATA)

                for (packageInfo in packages) {

                    if (packageInfo.flags and ApplicationInfo.FLAG_SYSTEM != 1) {

                        if (packageInfo.loadLabel(pm).toString() != "" && packageInfo.packageName != null) {
                            val bkapp = BlackListApp()
                            bkapp.nome = packageInfo.loadLabel(pm).toString()
                            bkapp.packages = packageInfo.packageName

                            Log.i(
                                "TRAFICC",
                                packageInfo.loadLabel(pm).toString().trim { it <= ' ' } + "/" + packageInfo.packageName.toString().trim { it <= ' ' })
                            Log.i("TRAFICC", "Bytes Enviados: " + TrafficStats.getUidRxBytes(packageInfo.uid))
                            Log.i("TRAFICC", "Bytes Recebidos: " + TrafficStats.getUidTxBytes(packageInfo.uid))
                            //Log.i("TRAFICC", "TOTAL: " + ((int)enviados + (int)recebidos));

                            listblapp.add(bkapp)
                        }
                    }
                }
            } catch (e: Exception) {
                LogTrace.logCatch(context, context.javaClass, e, true)
            }

            return listblapp
        }

        fun validaBlackList(bkapp: List<BlackListApp>, context: Context): List<BlackListApp> {

            /**
             * ARRAY LIST  - BLACKLIST
             */
            val bklistRetorno = ArrayList<BlackListApp>()
            /**
             * ARRAY LIST  - APPLICATION INFO
             */
            val listaAppInstaldos = ArrayList<ApplicationInfo>()

            try {

                val pm = context.packageManager
                /**
                 * PEGA A LISTA DE APPS INSTALADOS
                 */
                val packages = pm.getInstalledApplications(PackageManager.GET_META_DATA)

                /**
                 * ADICIONA TODAS OS APLICATIVOS INSTALADOS DENTRO DO ARRAY
                 */
                for (packageInfo in packages)
                    if (packageInfo.flags and ApplicationInfo.FLAG_SYSTEM != 1)
                        listaAppInstaldos.add(packageInfo)


                for (applicationInfo in listaAppInstaldos)
                    for (bkListApp in bkapp) {
                        //Log.i("TRAFICC", packageInfo.loadLabel(pm).toString().trim() + "/" + packageInfo.packageName.toString().trim());
                        //Log.i("TRAFICC", "Bytes Enviados: " + TrafficStats.getUidRxBytes(packageInfo.uid));
                        //Log.i("TRAFICC", "Bytes Recebidos: " + TrafficStats.getUidTxBytes(packageInfo.uid));

                        if (bkListApp.packages!!.toLowerCase().equals(applicationInfo.packageName.toLowerCase().trim { it <= ' ' }))
                            bklistRetorno.add(bkListApp)

                    }

            } catch (e: Exception) {
                LogTrace.logCatch(context, context.javaClass, e, true)
            }


            return bklistRetorno
        }


        fun validaBlackLIst(context: Context): Boolean {
            var nomeAppBloqueado = ""

            try {

                /**
                 * ABRE CONEXAO
                 */
                val db = SqliteDataBaseHelper.openDB(context)

                val bkdao = BlackListAppDAOHelper(db)
                val listaBk = bkdao.select()

                SqliteDataBaseHelper.closeDB(db)

                val retornoListaValida = validaBlackList(listaBk, context)

                if (retornoListaValida.isNotEmpty()) {
                    for (bkapp in retornoListaValida)
                        nomeAppBloqueado += bkapp.nome + "\n"

                    Alerta.show(
                        context,
                        context.resources.getString(R.string.msg_atencao),
                        context.resources.getString(R.string.msg_blacklist) + nomeAppBloqueado,
                        true
                    )

                    return false
                }
            } catch (e: Exception) {
                LogTrace.logCatch(context, context.javaClass, e, true)

            }

            return true

        }
    }

}