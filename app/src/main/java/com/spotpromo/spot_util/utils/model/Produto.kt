package com.spotpromo.spot_util.utils.model

import android.content.Context
import android.view.View
import android.widget.ImageView
import com.google.gson.annotations.SerializedName
import com.spotpromo.spot_util.R
import com.spotpromo.spot_util.utils.CarregaImagemDaUrl
import java.io.Serializable

class Produto : Serializable {
    @SerializedName("codSubBu")
    var codSubBu: Int? = null

    @SerializedName("codMarca")
    var codMarca: Int? = null

    @SerializedName("desMarca")
    var desMarca: String? = null

    @SerializedName("codCategoria")
    var codCategoria: Int? = null

    @SerializedName("desCategoria")
    var desCategoria: String? = null

    @SerializedName("codSubCategoria")
    var codSubCategoria: Int? = null

    @SerializedName("desSubCategoria")
    var desSubCategoria: String? = null

    @SerializedName("codSku")
    var codSku: Int? = null

    @SerializedName("desSku")
    var desSku: String? = null

    @SerializedName("precoSugerido")
    var precoSugerido: String? = null

    @SerializedName("flConcorrente")
    var flConcorrente: Int? = null

    @SerializedName("urlProduto")
    var urlProduto: String? = null

    @SerializedName("ordemShareCategoria")
    var ordemShareCategoria: Int? = null

    @SerializedName("qtdEstoqueMinimo")
    var qtdEstoqueMinimo: Int? = null

    var status: Int? = null
    var nivel: Int? = null
    var isSelected: Boolean = false

    var vinculo: Vinculo_Produto? = null



    fun get_title(context: Context): String? {
        when (nivel) {
            0 -> return String.format("%s", context.resources.getString(R.string.coleta_produto).toUpperCase())
            1 -> return String.format("%s", desCategoria)
        }
        return ""
    }

    fun get_sub_title(context: Context): String {
        when (nivel) {
            0 -> return String.format("%s", context.resources.getString(R.string.produto_categoria))
            1 -> return String.format("%s", context.resources.getString(R.string.produto_sku))
        }
        return ""
    }


    fun get_title_combe(context: Context): String? {
        when (nivel) {
            0 -> return String.format("%s", context.resources.getString(R.string.coleta_produto).toUpperCase())
            1 -> return String.format("%s", desCategoria)
            2 -> return String.format("%s", desSubCategoria)
            3 -> return String.format("%s", desMarca)
        }
        return ""
    }

    fun get_sub_title_combe(context: Context): String {
        when (nivel) {
            0 -> return String.format("%s", context.resources.getString(R.string.produto_categoria))
            1 -> return String.format("%s", context.resources.getString(R.string.produto_subcategoria))
            2 -> return String.format("%s", context.resources.getString(R.string.produto_marca))
            3 -> return String.format("%s", context.resources.getString(R.string.produto_sku))
        }
        return ""
    }

    fun get_texto(context: Context, vararg view : View): String {
        var sbHtml = StringBuilder()

        val color: String =
                when (status) {
                    0 -> context.resources.getString(R.string.chamado_vermelho)
                    1 -> context.resources.getString(R.string.chamado_amarelo)
                    else ->
                        context.resources.getString(R.string.chamado_verde)
                }

        when(status) {
            0 -> view[0].setBackgroundColor(context.resources.getColor(R.color.vermelho))
            1 -> view[0].setBackgroundColor(context.resources.getColor(R.color.amarelo))
            2 -> view[0].setBackgroundColor(context.resources.getColor(R.color.verde))
        }

        when (nivel) {
            0 -> {
                sbHtml.append(String.format("<b><font color='$color'>%s</font><b/>", desCategoria))
                view[2].visibility = View.GONE
            }
            1 -> {
                sbHtml.append(String.format("<b><font color='$color'>%s</font><b/>", desSku))
                if(flConcorrente == 1)
                    sbHtml.append("<br/><font color='red'><small>Concorrente</small></font>")

                if(!urlProduto.isNullOrEmpty() && urlProduto!!.length > 5) {
                    CarregaImagemDaUrl.carregaImagen_url(context, view[1] as ImageView, urlProduto!!)
                    view[2].visibility = View.VISIBLE
                }
            }
        }

        return sbHtml.toString()
    }

    fun get_texto_combe(context: Context, vararg view : View): String {
        var sbHtml = StringBuilder()

        val color: String =
            when (status) {
                0 -> context.resources.getString(R.string.chamado_vermelho)
                1 -> context.resources.getString(R.string.chamado_amarelo)
                else ->
                    context.resources.getString(R.string.chamado_verde)
            }

        when(status) {
            0 -> view[0].setBackgroundColor(context.resources.getColor(R.color.vermelho))
            1 -> view[0].setBackgroundColor(context.resources.getColor(R.color.amarelo))
            2 -> view[0].setBackgroundColor(context.resources.getColor(R.color.verde))
        }

        when (nivel) {
            0 -> {
                sbHtml.append(String.format("<b><font color='$color'>%s</font><b/>", desCategoria))
                view[2].visibility = View.GONE
            }
            1 -> {
                sbHtml.append(String.format("<b><font color='$color'>%s</font><b/>", desSubCategoria))
                view[2].visibility = View.GONE
            }
            2 -> {
                sbHtml.append(String.format("<b><font color='$color'>%s</font><b/>", desMarca))
                view[2].visibility = View.GONE
            }
            3 -> {
                sbHtml.append(String.format("<b><font color='$color'>%s</font><b/>", desSku))
                if(flConcorrente == 1)
                    sbHtml.append("<br/><font color='red'><small>Concorrente</small></font>")

                if(!urlProduto.isNullOrEmpty() && urlProduto!!.length > 5) {
                    CarregaImagemDaUrl.carregaImagen_url(context, view[1] as ImageView, urlProduto!!)
                    view[2].visibility = View.VISIBLE
                }
            }
        }

        return sbHtml.toString()
    }


}