package com.spotpromo.spot_util.utils

import android.Manifest
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.text.Html
import com.spotpromo.spot_util.utils.LogTrace
import com.spotpromo.spot_util.R
import java.util.ArrayList


object CheckReadPermission {
    var status = true
    var REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124

    fun show(activity: AppCompatActivity) {

        try {
            val permissionsNeeded = ArrayList<String>()

            val permissionsList = ArrayList<String>()
            if (!addPermission(permissionsList, Manifest.permission.RECORD_AUDIO, activity))
                permissionsNeeded.add(activity.resources.getString(R.string.permission_audio))
            if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE, activity))
                permissionsNeeded.add(activity.resources.getString(R.string.permission_sd_card_escrita))
            if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE, activity))
                permissionsNeeded.add(activity.resources.getString(R.string.permission_sd_card_leitura))
            if (!addPermission(permissionsList, Manifest.permission.CAMERA, activity))
                permissionsNeeded.add(activity.resources.getString(R.string.permission_camera))
            if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION, activity))
                permissionsNeeded.add(activity.resources.getString(R.string.permission_localizacao))
            if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE, activity))
                permissionsNeeded.add(activity.resources.getString(R.string.permission_telefone))

            if (permissionsList.size > 0) {
                if (permissionsNeeded.size > 0) {
                    // Need Rationale
                    val sbPermissao = StringBuilder()
                    sbPermissao.append(activity.resources.getString(R.string.msg_permissao))

                    for (i in permissionsNeeded.indices)
                        sbPermissao.append(String.format("<br>%s", permissionsNeeded[i]))

                    Alerta.show(activity,
                        "Attention",
                        Html.fromHtml(sbPermissao.toString()).toString(),
                        activity.resources.getString(R.string.btn_ok),
                        DialogInterface.OnClickListener { dialog, which ->
                            if (Build.VERSION.SDK_INT >= 23) {
                                activity.requestPermissions(
                                    permissionsList.toTypedArray(),
                                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS
                                )
                            }
                        },
                        false
                    )
                    return
                }

                if (Build.VERSION.SDK_INT >= 23) {
                    activity.requestPermissions(permissionsList.toTypedArray(), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
                }

                return
            }
        } catch (e: Exception) {
            LogTrace.logCatch(activity, activity.javaClass, e, true)
        }

        return
    }

    fun addPermission(permissionsList: MutableList<String>, permission: String, activity: AppCompatActivity): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission)
                // Check for Rationale Option
                if (!activity.shouldShowRequestPermissionRationale(permission))
                    return false
            }
        }
        return true
    }

    fun validaPermissao(activity: AppCompatActivity): Boolean {

        val permissionsList = ArrayList<String>()

        addPermission(permissionsList, Manifest.permission.RECORD_AUDIO, activity)
        addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE, activity)
        addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE, activity)
        addPermission(permissionsList, Manifest.permission.CAMERA, activity)
        addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION, activity)
        addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE, activity)

        return if (permissionsList.size <= 0) true else false

    }

}