package com.spotpromo.spot_util.utils.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Vinculo_Produto : Serializable {
    @SerializedName("codLoja")
    var codLoja: Int? = null
    @SerializedName("codSku")
    var codSku: Int? = null
    @SerializedName("precoMinimo")
    var precoMinimo: String? = null
    @SerializedName("precoMaximo")
    var precoMaximo: String? = null
    @SerializedName("flPreco")
    var flPreco: Int? = null
    @SerializedName("flFrentes")
    var flFrentes: Int? = null
    @SerializedName("flProfundidade")
    var flProfundidade: Int? = null
    @SerializedName("flPrecoPromocional")
    var flPrecoPromocional: Int? = null
}