package com.spotpromo.spot_util.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Message
import android.os.SystemClock
import android.util.Log

object CountUpTimer {

    var interval: Long = 0
    lateinit var context: Context
    private var base: Long = 0
    lateinit var service: ServicosTimer


    open fun start() {
        base = SystemClock.elapsedRealtime()
        handler.sendMessage(handler.obtainMessage(MSG))
    }

    open fun stop() {
        handler.removeMessages(MSG)
    }

    open fun reset() {
        synchronized(this) { base = SystemClock.elapsedRealtime() }
    }

    fun onTick(elapsedTime: Long) {}

    private val MSG = 1

    private val handler: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {
            synchronized(this@CountUpTimer) {
                try {

                    Log.e("SERVER_TIMER_CONT", "RODANDO")

                    if (!Util.checkAppIsRunning(context)) {

                        stop()

                        context.stopService(Intent(context, service::class.java))
                    }

                    val elapsedTime = System.currentTimeMillis()
                    onTick(elapsedTime)
                    sendMessageDelayed(obtainMessage(MSG), interval)
                } catch (err: Exception) {
                    err.printStackTrace()
                }

            }
        }
    }
}