package com.spotpromo.spot_util.utils

import android.content.Context
import android.text.Editable
import android.text.TextWatcher

class TextWatcherSolicitacaoMpdv(private val context: Context, private val position: Int) : TextWatcher {


    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        val onColetar = context as onColetar
        onColetar.onColetarSolicitacaoMpdv(position, if(s.isNullOrEmpty()) 0 else s.toString().toInt())
    }

    override fun afterTextChanged(s: Editable) {

    }

    interface onColetar {
        fun onColetarSolicitacaoMpdv(position: Int, qtd: Int)
    }
}