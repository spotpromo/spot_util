package com.spotpromo.spot_util.utils.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Departamento : Serializable {
    @SerializedName("codDepartamento")
    var codDepartamento: Int? = null
    @SerializedName("desDepartamento")
    var desDepartamento: String? = null
    @SerializedName("codTipoAtendimento")
    var codTipoAtendimento: Int? = null
    @SerializedName("desTipoDepartamento")
    var desTipoDepartamento: String? = null
    @SerializedName("flLoja")
    var flLoja: Int? = null
}