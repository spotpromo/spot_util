package com.spotpromo.spot_util.utils.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CodeData : Serializable {

    @SerializedName("codigo")
     var codigo: Int? = null
    @SerializedName("tipo")
     var tipo: String? = null
    @SerializedName("descricao")
     var descricao: String? = null

    var isSelected : Boolean = false
}