package com.spotpromo.spot_util.utils.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class RedeLoja : Serializable {
    @SerializedName("codRede")
    var codRede: Int? = null
    @SerializedName("desRede")
    var desRede: String? = null
    @SerializedName("codLoja")
    var codLoja: Int? = null
    @SerializedName("nomeLoja")
    var nomLoja: String? = null
    @SerializedName("endereco")
    var endereco: String? = null
    @SerializedName("cidade")
    var cidade: String? = null
}