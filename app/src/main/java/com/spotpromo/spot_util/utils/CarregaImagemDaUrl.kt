package com.spotpromo.spot_util.utils

import android.content.Context
import android.graphics.Bitmap
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import android.view.View
import android.widget.ImageView
import com.spotpromo.spot_util.utils.LogTrace
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.bumptech.glide.request.target.Target
import com.pnikosis.materialishprogress.ProgressWheel
import com.spotpromo.spot_util.R
import de.hdodenhof.circleimageview.CircleImageView
import java.io.File
import java.io.FileOutputStream

object CarregaImagemDaUrl : AppGlideModule() {
    fun buscar(mContext: Context, url: String, vararg v: View) {
        Glide.with(mContext)
            .asBitmap()
            .load(url)
            .apply(options(mContext))
            .listener(object : RequestListener<Bitmap> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any,
                    target: Target<Bitmap>,
                    isFirstResource: Boolean
                ): Boolean {
                    try {
                        if (v.size >= 2)
                            (v[1] as ProgressWheel).visibility = View.GONE

                        if (v[0] is ImageView)
                            (v[0] as ImageView).setImageDrawable(mContext.resources.getDrawable(R.drawable.ic_fechar))
                        else if (v[0] is CircleImageView)
                            if (v.size > 2) {
                                (v[2] as CoordinatorLayout).visibility = View.GONE
                            } else
                            (v[0] as CircleImageView).setImageDrawable(mContext.resources.getDrawable(R.drawable.ic_fechar))
                        else
                            (v[0] as com.ortiz.touchview.TouchImageView).setImageDrawable(
                                mContext.resources.getDrawable(
                                    R.drawable.ic_fechar
                                )
                            )

                    } catch (err: Exception) {
                        LogTrace.logCatch(mContext, mContext.javaClass, err, true)
                    }

                    return false
                }

                override fun onResourceReady(
                    resource: Bitmap,
                    model: Any,
                    target: Target<Bitmap>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    try {
                        if (v.size >= 2)
                            (v[1] as ProgressWheel).visibility = View.GONE

                        if (v[0] is ImageView)
                            (v[0] as ImageView).setImageBitmap(resource)
                        else if (v[0] is CircleImageView)
                            (v[0] as CircleImageView).setImageBitmap(resource)
                        else
                            (v[0] as com.ortiz.touchview.TouchImageView).setImageBitmap(resource)

                    } catch (err: Exception) {
                        LogTrace.logCatch(mContext, mContext.javaClass, err, true)
                    }

                    return false
                }
            })
            .submit()
    }

    fun options(context: Context): RequestOptions {

        val circularProgressDrawable =
            CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()

        return RequestOptions().fitCenter()
            .error(R.drawable.ic_fechar)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
            .centerCrop()
            .placeholder(circularProgressDrawable)
    }

    fun options_3(context: Context): RequestOptions {
        return RequestOptions().fitCenter()
            .error(R.drawable.ic_fechar)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
            .centerCrop()
    }


    @Throws(Exception::class)
    fun retornaImagem(mContext: Context, url: String): Bitmap {
        return Glide.with(mContext)
            .asBitmap()
            .load(url)
            .apply(options(mContext))
            .into(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
            .get()
    }

    @Throws(Exception::class)
    fun retornaImagem_2(mContext: Context, url: String, file : File) {

        Glide.with(mContext)
            .asBitmap()
            .load(url)
            .apply(options_3(mContext))
            .listener(object : RequestListener<Bitmap> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any,
                    target: Target<Bitmap>,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Bitmap,
                    model: Any,
                    target: Target<Bitmap>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    try {
                        val out = FileOutputStream(file)
                        resource.compress(Bitmap.CompressFormat.JPEG, 90, out)
                        out.flush()
                        out.close()
                    } catch (err : java.lang.Exception) {
                        err.printStackTrace()
                    }

                    return false
                }
            })
            .submit()
    }

    fun carregaImagen(context: Context, v: CircleImageView, url : String) {

        Glide.with(context)
            .asBitmap()
            .load(url)
            .apply(options(context))
            .into(v)

    }

    fun carregaImagen(context: Context, v: ImageView, url : String) {

        Glide.with(context)
            .asBitmap()
            .load(url)
            .apply(options(context))
            .into(v)

    }

    fun options_2(context: Context): RequestOptions {

        val circularProgressDrawable =
            CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth = 6f
        circularProgressDrawable.centerRadius = 16f
        circularProgressDrawable.start()

        val mOption = RequestOptions()
        mOption
            .fitCenter()
            .error(R.drawable.ic_fechar)
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .placeholder(circularProgressDrawable)
        return mOption
    }


    fun carregaImagen_url(context: Context, v: ImageView, url: String) {

        Glide.with(context)
            .asBitmap()
            .load(url)
            .apply(options_2(context))
            .apply(RequestOptions.circleCropTransform())
            .dontAnimate()
            .into(object : BitmapImageViewTarget(v) {
                override fun setResource(resource: Bitmap?) {
                    super.setResource(resource)
                }
            })

    }

    fun carregaImagen_galeria(context: Context, v: ImageView, url: String) {

        Glide.with(context)
            .asBitmap()
            .load(File(url))
            .apply(options_2(context))
            .apply(RequestOptions.circleCropTransform())
            .dontAnimate()
            .into(object : BitmapImageViewTarget(v) {
                override fun setResource(resource: Bitmap?) {
                    super.setResource(resource)
                }
            })

    }
}