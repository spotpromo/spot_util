package com.spotpromo.spot_util.utils

import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import android.util.Log

open class SupportApplication : MultiDexApplication(){

    override fun onCreate() {
        super.onCreate()
        inicia()
    }

    private fun inicia() {
        try {
            MultiDex.install(this)
        } catch (error : Throwable) {
            Log.e(SupportApplication::class.java.simpleName, "Erro ao instalar multiDex!!!")
            LogTrace.logCatch(this, this.javaClass, error, false)
        }

    }
}