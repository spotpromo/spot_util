package com.spotpromo.spot_util.utils.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
@Parcelize
class BlackListAppNew(
    @Json(name = "nomeApp") var nome: String? = null,
    @Json(name = "packge") var packages: String? = null
) : Parcelable