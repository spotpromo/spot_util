package com.spotpromo.spot_util.utils

import android.os.Build
import androidx.annotation.RequiresApi
import java.text.SimpleDateFormat

class DocumentoValido {

    companion object {
        // CPF
        private val weightSsn = intArrayOf(11, 10, 9, 8, 7, 6, 5, 4, 3, 2)

        // CNPJ
        private val weightTin = intArrayOf(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2)


        fun unmask(s: String): String {
            return s.replace("[.]".toRegex(), "").replace("[-]".toRegex(), "")
                .replace("[/]".toRegex(), "").replace("[(]".toRegex(), "")
                .replace("[)]".toRegex(), "").replace(" ", "")
                .replace(",", "")
        }

        @Throws(Exception::class)
        private fun calculate(str: String, weight: IntArray): Int {
            var sum = 0
            var i = str.length - 1
            var digit: Int
            while (i >= 0) {
                digit = Integer.parseInt(str.substring(i, i + 1))
                sum += digit * weight[weight.size - str.length + i]
                i--
            }
            sum = 11 - sum % 11
            return if (sum > 9) 0 else sum
        }

        /**
         * Valida CPF
         *
         * @param ssn
         * @return
         */
        @Throws(Exception::class)
        fun isValidSsn(ssn: String?): Boolean {
            if (ssn == null || ssn.length != 11 || ssn.matches((ssn[0] + "{11}").toRegex()))
                return false

            val digit1 = calculate(ssn.substring(0, 9), weightSsn)
            val digit2 = calculate(ssn.substring(0, 9) + digit1, weightSsn)
            return ssn == ssn.substring(0, 9) + digit1.toString() + digit2.toString()
        }

        /**
         * Valida CNPJ
         *
         * @param tin
         * @return
         */
        @Throws(Exception::class)
        fun isValidTin(tin: String?): Boolean {
            if (tin == null || tin.length != 14 || tin.matches((tin[0] + "{14}").toRegex()))
                return false

            val digit1 = calculate(tin.substring(0, 12), weightTin)
            val digit2 = calculate(tin.substring(0, 12) + digit1, weightTin)
            return tin == tin.substring(0, 12) + digit1.toString() + digit2.toString()
        }


        @RequiresApi(api = Build.VERSION_CODES.N)
        @Throws(Exception::class)
        fun validaData(data: String): Boolean {

            try {
                val df = SimpleDateFormat("MM/dd/yyyy") ?: throw NullPointerException("Error format date")


                df.isLenient = false
                df.parse(data)
            } catch (e: Exception) {
                return false
            }

            return true
        }
    }

}