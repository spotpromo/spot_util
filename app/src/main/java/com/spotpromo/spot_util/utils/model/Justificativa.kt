package com.spotpromo.spot_util.utils.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Justificativa : Serializable {
    @SerializedName("codJustificativa")
    var codJustificativa: Int? = null
    @SerializedName("desJustificativa")
    var desJustificativa: String? = null
    @SerializedName("flPesquisaPendente")
    var flPesquisaPendente: Int? = null
    @SerializedName("flFoto")
    var flFoto: Int? = null
    @SerializedName("flLoja")
    var flLoja: Int? = null
}