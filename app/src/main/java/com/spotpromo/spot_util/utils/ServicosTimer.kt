package com.spotpromo.spot_util.utils

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log


class ServicosTimer : Service() {


    override fun onCreate() {
        super.onCreate()
        startService()
    }

    fun startService() {

        try {
            Log.e("SERVER_TIMER", "INICIADO")
            CountUpTimer.interval = 1000
            CountUpTimer.context = applicationContext
            CountUpTimer.service = this
            CountUpTimer.start()
        } catch (er: Exception) {
            er.printStackTrace()
        }

    }


    override fun onDestroy() {
        super.onDestroy()
        Log.e("SERVER_TIMER", "SERVICO DESTRUIDO")

        val intentBroadcast = Intent(this, SensorRestartServico::class.java)
        this.sendBroadcast(intentBroadcast)
        CountUpTimer.stop()


    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

}
