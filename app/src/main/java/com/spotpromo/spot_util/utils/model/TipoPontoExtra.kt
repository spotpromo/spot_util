package com.spotpromo.spot_util.utils.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
class TipoPontoExtra(
    @Json(name = "codTipoPontoExtra") var codTipoPontoExtra: Int? = null,
    @Json(name = "desTipoPontoExtra") var desTipoPontoExtra: String? = null,
    @Json(name = "qtdFotoMin") var qtdFotoMin: Int? = null,
    @Json(name = "qtdFotoMax") var qtdFotoMax: Int? = null,
    @Json(name = "flFrentes") var flFrentes: Int? = null
) : Parcelable