package com.spotpromo.spot_util.utils

enum class EnumTipo private constructor(private val nome: String) {

    TEXT("Texto"),
    SELECT("Seleção"),
    PHOTO("Foto");

    override fun toString(): String {
        return nome
    }

    enum class EnumTipoText private constructor(private val tipoTexto: String) {

        NORMAL("Normal"),
        NUMERIC("Numerico"),
        CPF("CPF"),
        CNPJ("CNPJ"),
        DATA("Data"),
        PRECO("Preco"),
        PHONE("Telefone"),
        PERCENTUAL("Percentual"),
        CEP("Cep"),
        DECIMAL("Decimal");

        override fun toString(): String {
            return tipoTexto
        }
    }

    enum class EnumTipoSelect private constructor(private val nomeSelect: String) {
        CHECKBOX("Multipla Seleção"),
        RADIO("Seleção Simples");

        override fun toString(): String {
            return nomeSelect
        }
    }

    enum class EnumTipoPhoto private constructor(private val nomeFoto: String) {
        FOTOCAMERA("Foto Camera"),
        FOTOGALLERY("Foto Galeria");

        override fun toString(): String {
            return nomeFoto
        }

    }

}