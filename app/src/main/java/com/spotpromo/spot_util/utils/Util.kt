package com.spotpromo.spot_util.utils

import android.app.Activity
import android.app.ActivityManager
import android.app.AlertDialog
import android.content.*
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.BatteryManager
import android.os.Build
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.Settings
import androidx.annotation.RequiresApi
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.RecyclerView
import android.text.format.DateFormat
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.*
import com.ortiz.touchview.TouchImageView
import com.spotpromo.spot_util.R
import com.spotpromo.spot_util.utils.*
import com.spotpromo.spot_util.utils.config.SpotUtilConfig
import com.spotpromo.spot_util.utils.model.*
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.layout_toast.view.*
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object Util {

    @Throws(Exception::class)
    fun validaEditText(edt: EditText, context: Context): Boolean {
        if (edt.text.toString().length <= 0) {
            edt.setError(context.resources.getString(R.string.msg_campo_obrigatorio))
            return false
        }

        return true
    }


    @Throws(Exception::class)
    fun validaEditTextInteiro(edt: EditText, context: Context, valida_zero: Boolean): Boolean {
        if (edt.text.toString().isEmpty()) {
            edt.setError(context.resources.getString(R.string.msg_campo_obrigatorio))
            return false
        }

        if (valida_zero && edt.text.toString().toInt() == 0) {
            edt.setError(context.resources.getString(R.string.msg_valor_maior_zero))
            return false
        }

        return true
    }

    @Throws(Exception::class)
    fun validaEditTextDouble(edt: EditText, context: Context, valida_zero: Boolean): Boolean {
        if (edt.text.toString().isEmpty()) {
            edt.setError(context.resources.getString(R.string.msg_campo_obrigatorio))
            return false
        }

        if (valida_zero && edt.text.toString().toDouble() == 0.0) {
            edt.setError(context.resources.getString(R.string.msg_valor_maior_zero))
            return false
        }

        return true
    }

    /**
     * VALIDA EDIT TEXT
     *
     * @param edt
     * @param context
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    fun validaEditTextTamanho(edt: EditText, context: Context, tamanho: Int): Boolean {
        if (edt.text.toString().length <= 0) {
            edt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
            return false
        } else if (edt.text.toString().length < tamanho) {
            edt.error = context.resources.getString(R.string.minimo_caracteres)
                .replace("_CARAC", tamanho.toString())
            return false
        }

        return true
    }

    /**
     * VALIDA EDIT TEXT
     *
     * @param edt
     * @param context
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    fun validaEditTextPreco(edt: EditText, context: Context): Boolean {
        val ln = edt.parent as LinearLayout
        if (ln.visibility == View.VISIBLE && edt.text.toString().isEmpty()) {
            edt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
            edt.isFocusable = true
            edt.isFocusableInTouchMode = true
            edt.requestFocus()
            return false
        }

        /**
         * VALIDA VALOR PREÇO == 0.00
         */
        val preco =
            edt.text.toString().replace("R$", "").replace("$", "").replace(
                ".",
                ""
            ).replace(",", ".").toDouble()

        if (preco == 0.0 || preco == 0.00) {
            Alerta.show(
                context,
                context.resources.getString(R.string.msg_atencao),
                context.resources.getString(R.string.msg_preco_maior_zero),
                true
            )
            edt.requestFocus()
            return false
        }
        return true

    }


    /**
     * VALIDA EDIT TEXT
     *
     * @param edt
     * @param context
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    fun validaEditTextPreco(edt: EditText, context: Context, zero: Boolean): Boolean {
        val ln = edt.parent as LinearLayout
        if (ln.visibility == View.VISIBLE && edt.text.toString().isEmpty()) {
            edt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
            edt.isFocusable = true
            edt.isFocusableInTouchMode = true
            edt.requestFocus()
            return false
        }

        /**
         * VALIDA VALOR PREÇO == 0.00
         */
        val preco =
            edt.text.toString().replace("R$", "").replace("$", "").replace(
                ".",
                ""
            ).replace(",", ".").toDouble()

        if (zero && (preco == 0.0 || preco == 0.00)) {
            Alerta.show(
                context,
                context.resources.getString(R.string.msg_atencao),
                context.resources.getString(R.string.msg_preco_maior_zero),
                true
            )
            edt.requestFocus()
            return false
        }
        return true

    }

    /**
     * VALIDA EDIT TEXT
     *
     * @param edt
     * @param context
     * @return
     */
    @Throws(Exception::class)
    fun validaEditTextData(edt: EditText, context: Context, tamanho: Int?): Boolean {
        if (edt.text.toString().isEmpty()) {
            edt.error = context.resources.getString(R.string.campo_obrigatorio)
            return false
        } else if (edt.text.toString().length < tamanho!!) {
            edt.error = context.resources.getString(R.string.min_caracteres_data)
                .replace("_CARACTERES", tamanho.toString())
            return false
        } else if (edt.text.toString().length >= tamanho) {
            try {
                val df = SimpleDateFormat("dd/MM/yyyy")
                df.isLenient = false
                df.parse(edt.text.toString())

                return true
            } catch (e: ParseException) {
                edt.error = context.resources.getString(R.string.valid_caracteres_data)
                return false
            }

        }

        return true
    }

    /**
     * RETORNA NIVEL DE BATERIA
     *
     * @param context
     * @return
     */
    fun getBatteryLevel(context: Context): Float {
        val batteryIntent =
            context.registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
        val level = batteryIntent!!.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
        val scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)

        // Error checking that probably isn't needed but I added just in case.
        return if (level == -1 || scale == -1) {
            50.0f
        } else level.toFloat() / scale.toFloat() * 100.0f

    }

    @Throws(Exception::class)
    fun retornaDataFoto(dateFoto: String?): String {
        var formattedDateString = ""

        if (dateFoto != null && dateFoto.isNotEmpty()) {
            val file = File(dateFoto)

            if (file != null && file.isFile) {

                /**
                 * PEGA DATA DA FOTO
                 */
                val lastModified = Date(file.lastModified())
                val formatter = SimpleDateFormat("yyyy-MM-dd kk:mm:ss")

                formattedDateString = FormataData[FormataData.getCalendar(
                    formatter.format(lastModified),
                    FormataData.SQL_2
                ), FormataData.SQL]

            } else {
                /**
                 * SE NÃO ACHAR A DATA ELE INSERI UMA ATUAL
                 */
                formattedDateString = FormataData.retornaDataFormat(FormataData.SQL_2)

            }
        } else {
            /**
             * SE NÃO ACHAR A DATA ELE INSERI UMA ATUAL
             */
            formattedDateString = FormataData.retornaDataFormat(FormataData.SQL_2)


        }

        return formattedDateString
    }

    /**
     * NAPER
     */
    fun retonarNomeFoto(caminhoFoto: String): String {
        val foto = caminhoFoto.split("/")

        return foto[foto.size - 1]
    }

    /**
     * RETORNA MIMETYPE
     *
     * @param file
     * @param context
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    fun retornaMimeType(file: File, context: Context): String? {

        var uri: Uri? = null

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uri =
                FileProvider.getUriForFile(context, SpotUtilConfig.appID + ".provider", file)
        } else {
            uri = Uri.fromFile(file)
        }

        val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri!!.toString())

        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension)
    }

    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerDepartamento(spn: Spinner, context: Context): Boolean {
        val departamento = spn.selectedItem as Departamento

        if (departamento.codDepartamento == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }

    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerJustificativa(spn: Spinner, context: Context): Boolean {
        val justificativa = spn.selectedItem as Justificativa

        if (justificativa.codJustificativa == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }


    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerMarca(spn: Spinner, context: Context): Boolean {
        val produto = spn.selectedItem as Produto

        if (produto.codMarca == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }

    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerCategoria(spn: Spinner, context: Context): Boolean {
        val produto = spn.selectedItem as Produto

        if (produto.codCategoria == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }

    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerSku(spn: Spinner, context: Context): Boolean {
        val produto = spn.selectedItem as Produto

        if (produto.codSku == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }


    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerTipoFoto(spn: Spinner, context: Context): Boolean {
        val tipofoto = spn.selectedItem as TipoFoto

        if (tipofoto.codTipoExecucao == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }

    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerCodeData(spn: Spinner, context: Context): Boolean {
        val codedata = spn.selectedItem as CodeData

        if (codedata.codigo == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }

    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerBandeira(spn: Spinner, context: Context): Boolean {
        val bandeira = spn.selectedItem as BandeiraUfCidade

        if (bandeira.codBandeira == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }

    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerTipoPontoExtra(spn: Spinner, context: Context): Boolean {
        val tipope = spn.selectedItem as TipoPontoExtra

        if (tipope.codTipoPontoExtra == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }

    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerUf(spn: Spinner, context: Context): Boolean {
        val bandeira = spn.selectedItem as BandeiraUfCidade

        if (bandeira.codUf == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }

    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerCidade(spn: Spinner, context: Context): Boolean {
        val bandeira = spn.selectedItem as BandeiraUfCidade

        if (bandeira.codCidade == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }

    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerTipoAtendimento(spn: Spinner, context: Context): Boolean {
        val departamento = spn.selectedItem as Departamento

        if (departamento.codTipoAtendimento == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }

    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerRede(spn: Spinner, context: Context): Boolean {
        val cd = spn.selectedItem as RedeLoja

        if (spn.visibility == View.VISIBLE && cd.codRede == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.isFocusable = true
                txt.isFocusableInTouchMode = true
                txt.requestFocus()

            }

            return false
        }

        return true
    }

    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerRedeLoja(spn: Spinner, context: Context): Boolean {
        val cd = spn.selectedItem as RedeLoja

        if (spn.visibility == View.VISIBLE && cd.codLoja == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.isFocusable = true
                txt.isFocusableInTouchMode = true
                txt.requestFocus()

            }

            return false
        }

        return true
    }

    /**
     * VERIFICA DATA E HORA AUTOMATICA
     *
     * @param context
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    fun dataHoraAutomaticas(context: Context): Boolean {

        val timeSettings: Int?
        val timeZona: Int?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            timeSettings =
                Settings.Global.getInt(context.contentResolver, Settings.Global.AUTO_TIME, 0)
            timeZona =
                Settings.Global.getInt(context.contentResolver, Settings.Global.AUTO_TIME_ZONE, 0)
        } else {
            timeSettings = android.provider.Settings.System.getInt(
                context.contentResolver,
                android.provider.Settings.System.AUTO_TIME_ZONE,
                0
            )
            timeZona = android.provider.Settings.System.getInt(
                context.contentResolver,
                android.provider.Settings.System.AUTO_TIME,
                0
            )
        }

        if (timeSettings == 0 || timeZona == 0) {
            Alerta.show(
                context,
                context.resources.getString(R.string.msg_atencao),
                context.resources.getString(R.string.data_hora_aumtomaticas),
                context.resources.getString(R.string.btn_configurar),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    try {
                        context.startActivity(Intent(Settings.ACTION_DATE_SETTINGS))
                    } catch (err: Exception) {
                        LogTrace.logCatch(context, context.javaClass, err, true)
                    }
                },
                false
            )
            return false
        }

        return true
    }

    /**
     * VERIFICA FORMATO 24HRS
     *
     * @param context
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    fun format24Hours(context: Context): Boolean {

        val time24Horas: Boolean
        time24Horas = DateFormat.is24HourFormat(context)

        if (!time24Horas) {
            Alerta.show(
                context,
                context.resources.getString(R.string.msg_atencao),
                context.resources.getString(R.string.data_formato_hora),
                context.resources.getString(R.string.btn_configurar),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    try {
                        context.startActivity(Intent(Settings.ACTION_DATE_SETTINGS))
                    } catch (err: Exception) {
                        LogTrace.logCatch(context, context.javaClass, err, true)
                    }
                },
                false
            )

            return false
        }

        return true
    }

    @Throws(Exception::class)
    fun validaImagemPerfil(context: Context, foto: String, codPessoa: Int?): Boolean {
        val sdcard = context.getExternalFilesDir(null)
        val SDDIRETORIO =
            File(
                sdcard!!.absolutePath + File.separator + SpotUtilConfig.caminho_db

            )

        if (!SDDIRETORIO.isDirectory)
            SDDIRETORIO.mkdirs()

        val mDirectoryPessoa =
            File(SDDIRETORIO.absolutePath + File.separator + codPessoa.toString())

        if (!mDirectoryPessoa.isDirectory)
            mDirectoryPessoa.mkdirs()

        val mFoto = File(mDirectoryPessoa.absolutePath, foto)
        return if (!mFoto.isFile) {
            val url = SpotUtilConfig.url_perfil
                .replace("_CODPESSOA", codPessoa.toString()).replace("_IMAGEM", foto)

            CarregaImagemDaUrl.retornaImagem_2(context, url, mFoto)

            true

        } else
            true


        return false
    }

    /**
     * RETORNA HASH MAP
     *
     * @param params
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    fun retornaHashJson(type: String, vararg params: Any): HashMap<String, String> {
        val mHash = HashMap<String, String>()

        if (params.size > 0) {
            for (`object` in params) {
                val key = (`object` as String).split(type.toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()
                if (key.size >= 2)
                    mHash[key[0]] = key[1]

            }
        }
        return mHash
    }

    /**
     * MOSTRAR IMAGEM
     *
     * @param campoFoto
     * @param foto
     * @param codPessoa
     * @throws Exception
     */
    @Throws(Exception::class)
    fun mostraImagemUsuario(
        context: Context,
        campoFoto: CircleImageView,
        foto: String,
        codPessoa: Int?
    ) {
        val sdcard = context.getExternalFilesDir(null)
        val SDDIRETORIO =
            File(sdcard!!.absolutePath + File.separator + SpotUtilConfig.caminho_db)
        val mediaStorageDir = File(SDDIRETORIO.absolutePath, codPessoa.toString())
        val file = File(mediaStorageDir.absolutePath, foto)
        if (file != null && file.isFile) {
            val bitmap =
                SupportImagem.ajustOrientation(
                    file,
                    300,
                    300
                )//Bitmap.createScaledBitmap(bitmap, 300, 300, true);
            //o BitmapReduzido é apenas para setar ele no ImageView

            campoFoto.setImageBitmap(bitmap)
        }

    }

    /**
     * MOSTRAR IMAGEM
     *
     * @param campoFoto
     * @param caminhoFoto
     * @throws Exception
     */
    @Throws(Exception::class)
    fun mostraImagem(campoFoto: View, caminhoFoto: String?) {
        if (caminhoFoto != null) {
            val file = File(caminhoFoto)
            if (file != null && file.isFile) {
                val bitmap =
                    SupportImagem.ajustOrientation(
                        file,
                        640,
                        480
                    )//Bitmap.createScaledBitmap(bitmap, 300, 300, true);
                //o BitmapReduzido é apenas para setar ele no ImageView
                if (campoFoto is CircleImageView)
                    campoFoto.setImageBitmap(bitmap)
                else if (campoFoto is ImageView)
                    campoFoto.setImageBitmap(bitmap)

                campoFoto.tag = caminhoFoto
            }
        }
    }

    /**
     * MENSAGEM SNACK
     */

    @Throws(Exception::class)
    fun mensagemSnack(v: View, mensagem: String, idColor: Int?, icone: Int?) {
        val mSnake = Snackbar.make(v, mensagem, Snackbar.LENGTH_SHORT)
        val vsnaker = mSnake.view
        vsnaker.setBackgroundResource(idColor!!)
        val txtSnaker =
            vsnaker.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
        txtSnaker.setCompoundDrawablesWithIntrinsicBounds(0, 0, icone ?: 0, 0)
        txtSnaker.gravity = Gravity.CENTER
        txtSnaker.maxLines = 4
        mSnake.duration = 10000
        mSnake.setActionTextColor(v.context.resources.getColor(R.color.branco))
        mSnake.setAction("OK") { mSnake.dismiss() }
        mSnake.show()
    }


    /**
     * RETORNA CAMERA DO CELULAR
     *
     * @param activity
     * @param tipo
     * @return
     */
    @Throws(Exception::class)
    fun retornaCameraCelStringFoto(activity: Activity, codPessoa: Int?, tipo: Int?): String? {
        val file: File
        var filePath: String? = null

        filePath = CriarFileTemporarioStringFoto(codPessoa, activity)
        file = File(filePath)

        val outputFileUri =
            FileProvider.getUriForFile(activity, SpotUtilConfig.appID  + ".provider", file)

        if (outputFileUri != null) {
            activity.startActivityForResult(
                Intent(MediaStore.ACTION_IMAGE_CAPTURE).putExtra(
                    MediaStore.EXTRA_OUTPUT,
                    outputFileUri
                ), tipo!!
            )
        } else {
            Alerta.show(
                activity,
                activity.resources.getString(R.string.msg_atencao),
                activity.resources.getString(R.string.msg_erro_foto),
                true
            )
        }

        return filePath
    }

    /**
     * RETORNA CAMERA DO CELULAR
     *
     * @param activity
     * @param context
     * @param nomeDiretorio
     * @param tipo
     * @return
     */
    fun retornaCameraCelString(
        activity: Activity,
        context: Context,
        nomeDiretorio: String,
        tipo: Int?
    ): String? {
        val file: File
        var filePath: String? = null
        try {


            filePath = CriarFileTemporarioString(nomeDiretorio, context)
            file = File(filePath)

            val outputFileUri =
                FileProvider.getUriForFile(context, SpotUtilConfig.appID  + ".provider", file)

            if (outputFileUri != null) {
                activity.startActivityForResult(
                    Intent(MediaStore.ACTION_IMAGE_CAPTURE).putExtra(
                        MediaStore.EXTRA_OUTPUT,
                        outputFileUri
                    ), tipo!!
                )
            } else {
                Alerta.show(context, "Atenção.", "Erro ao capturar a foto", false)
            }

        } catch (e: Exception) {
            LogTrace.logCatch(context, activity.javaClass, e, true)
        }

        return filePath
    }

    /**
     * RETORNA CAMERA DO CELULAR
     *
     * @param activity
     * @param context
     * @param nomeDiretorio
     * @param tipo
     * @return
     */
    fun retornaCameraCelStringFragment(
        fragment: Fragment,
        context: Context,
        nomeDiretorio: String,
        tipo: Int?
    ): String? {
        val file: File
        var filePath: String? = null
        try {


            filePath = CriarFileTemporarioString(nomeDiretorio, context)
            file = File(filePath)


            var outputFileUri : Uri
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                outputFileUri =
                    FileProvider.getUriForFile(
                        context,
                        SpotUtilConfig.appID  + ".provider",
                        file
                    )
            }else {
                outputFileUri = Uri.fromFile(file)
            }

            if (outputFileUri != null) {
                fragment.startActivityForResult(
                    Intent(MediaStore.ACTION_IMAGE_CAPTURE).putExtra(
                        MediaStore.EXTRA_OUTPUT,
                        outputFileUri
                    ), tipo!!
                )
            } else {
                Alerta.show(context, "Atenção.", "Erro ao capturar a foto", false)
            }

        } catch (e: Exception) {
            LogTrace.logCatch(context, context.javaClass, e, true)
        }

        return filePath
    }

    /**
     * CRIA ARQUIVO TEMPORARIO STRING
     *
     * @param nomeDiretorio
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    fun CriarFileTemporarioString(nomeDiretorio: String, context: Context): String? {

        val sdcard = context.getExternalFilesDir(null)
        val SDDIRETORIO =
            File(sdcard!!.absolutePath + File.separator + SpotUtilConfig.caminho_db)

        if (!SDDIRETORIO.isDirectory)
            SDDIRETORIO.mkdirs()

        val mediaStorageDir = File(SDDIRETORIO.absolutePath + File.separator + nomeDiretorio)

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val mediaFile: File
        mediaFile =
            File(mediaStorageDir.path + File.separator + nomeDiretorio + "_" + timeStamp + ".jpg")

        return mediaFile.absolutePath
    }

    /**
     * CRIA ARQUIVO TEMPORARIO STRING
     *
     * @param codPessoa
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    fun CriarFileTemporarioStringFoto(codPessoa: Int?, activity: Activity): String? {

        val sdcard = activity.getExternalFilesDir(null)
        val SDDIRETORIO =
            File(sdcard!!.absolutePath + File.separator + SpotUtilConfig.caminho_db)
        val mediaStorageDir = File(SDDIRETORIO.absolutePath, codPessoa.toString())
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }

        val timeStamp =
            SimpleDateFormat(
                FormataData.retornaDataFormat(FormataData.DataFoto),
                Locale.getDefault()
            ).format(Date())
        val mediaFile: File
        mediaFile =
            File(mediaStorageDir.path + File.separator + codPessoa.toString() + "_" + timeStamp + "_.jpg")

        return mediaFile.absolutePath
    }

    /**
     * MOSTRA IMAGEM DIALOG
     *
     * @param context
     * @param imagemBitmap
     */
    @Throws(Exception::class)
    fun showImageGallery(context: Context, imagemBitmap: Bitmap, vB: View?) {

        val params =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                1f
            )

        val mDialog: AlertDialog?
        val builder = AlertDialog.Builder(context)

        val inflater = (context as Activity).layoutInflater
        val dialogView = inflater.inflate(R.layout.layout_dialog_show_image, null)

        val timageView = dialogView.findViewById(R.id.timageproduto) as TouchImageView
        val fechar = dialogView.findViewById(R.id.fechar) as ImageView

        timageView.setImageBitmap(imagemBitmap)
        //timageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        //dialogView.setLayoutParams(params);
        builder.setView(dialogView)

        mDialog = builder.create()
        mDialog!!.show()

        fechar.setOnClickListener {
            if (vB != null)
                vB.isClickable = true

            if (mDialog != null && mDialog.isShowing)
                mDialog.dismiss()
        }
    }


    /**
     * MOSTRA IMAGEM DIALOG
     *
     * @param context
     * @param imagemBitmap
     */
    @Throws(Exception::class)
    fun showImageGallery(context: Context, url: String, vB: View?) {

        val params =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                1f
            )

        val mDialog: AlertDialog?
        val builder = AlertDialog.Builder(context)

        val inflater = (context as Activity).layoutInflater
        val dialogView = inflater.inflate(R.layout.layout_dialog_show_image, null)

        val timageView = dialogView.findViewById(R.id.timageproduto) as TouchImageView
        val fechar = dialogView.findViewById(R.id.fechar) as ImageView

        CarregaImagemDaUrl.carregaImagen(context, timageView, url)

        builder.setView(dialogView)

        mDialog = builder.create()
        mDialog!!.show()

        fechar.setOnClickListener {
            if (vB != null)
                vB.isClickable = true

            if (mDialog != null && mDialog.isShowing)
                mDialog.dismiss()
        }
    }

    /**
     * MOSTRA IMAGEM DIALOG
     *
     * @param context
     * @param imagemBitmap
     */
    @Throws(Exception::class)
    fun showImageGallery_2(context: Context, url: String, vB: View?, tipo: Int) {

        val params =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                1f
            )

        val mDialog: AlertDialog?
        val builder = AlertDialog.Builder(context)

        val inflater = (context as Activity).layoutInflater
        val dialogView = inflater.inflate(R.layout.layout_dialog_show_image, null)

        val timageView = dialogView.findViewById(R.id.timageproduto) as TouchImageView
        val fechar = dialogView.findViewById(R.id.fechar) as ImageView

        if (tipo == 1)
            CarregaImagemDaUrl.carregaImagen(context, timageView, url)
        else
            mostraImagem(timageView, url)

        builder.setView(dialogView)

        mDialog = builder.create()
        mDialog!!.show()

        fechar.setOnClickListener {
            if (vB != null)
                vB.isClickable = true

            if (mDialog != null && mDialog.isShowing)
                mDialog.dismiss()
        }
    }


    /**
     * MOSTRA IMAGEM DIALOG
     *
     * @param context
     * @param imagemBitmap
     */

    /**
     * ABRE GALERIA
     *
     * @param mFrag
     * @return
     */
    fun abrirGaleria(mFrag: Fragment, retorno: Int?) {

        try {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            mFrag.startActivityForResult(
                Intent.createChooser(intent, "Select the photo"),
                retorno!!
            )
        } catch (e: Exception) {
            Log.v("TAG", "Não é possível selecionar a foto.")
        }

    }

    /**
     * ABRE GALERIA
     *
     * @param mFrag
     * @return
     */
    fun abrirGaleria(activity: Activity, retorno: Int?) {

        try {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            activity.startActivityForResult(
                Intent.createChooser(intent, "Select the photo"),
                retorno!!
            )
        } catch (e: Exception) {
            Log.v("TAG", "Não é possível selecionar a foto.")
        }

    }

    /**
     * METODO IMPLODE STRING
     *
     * @param array
     * @param separador
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    fun implodeString(array: Array<String>, separador: String): String {

        if (array.isEmpty())
            return ""

        if (array.size < 2)
            return array[0]

        val stringBuffer = StringBuffer()
        for (i in array.indices) {
            if (i != 0)
                stringBuffer.append(separador)
            stringBuffer.append(array[i])
        }

        return stringBuffer.toString()
    }

    /**
     * METODO IMPLODE STRING
     *
     * @param array
     * @param separador
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    fun implodeString_2(array: List<String>, separador: String): String {

        if (array.isEmpty())
            return ""

        if (array.size < 2)
            return array[0]

        val stringBuffer = StringBuffer()
        for (i in array.indices) {
            if (i != 0)
                stringBuffer.append(separador)
            stringBuffer.append(array[i])
        }

        return stringBuffer.toString()
    }

    /**
     * Manipula progress bar
     *
     * @param context
     * @param mensagem
     * @param opcao    1 para alterar o texto
     * 2 para alterar o progresso da barra
     * 3 para alterar o maximo de progresso da barra
     */
    fun manipulaLoading(context: Context, valor: Int, mensagem: String, opcao: Int) {

        (context as Activity).runOnUiThread {
            when (opcao) {
                1 -> {
                    Loading.setText(mensagem.trim { it <= ' ' })
                }
                2 -> {
                    Loading.setProgressValue(valor)
                }
                3 -> {
                    Loading.setProgressMax(valor)
                }
            }
        }
    }

    @Throws(Exception::class)
    fun mostraImagemDrawable(campoFoto: ImageView, mDraw: Drawable) {

        //o BitmapReduzido é apenas para setar ele no ImageView
        campoFoto.scaleType = ImageView.ScaleType.CENTER_CROP
        campoFoto.setImageDrawable(mDraw)
        //campoFoto.setScaleType(ImageView.ScaleType.FIT_XY);


    }

    /**
     * RETORNA PATH DA GALERIA
     *
     * @param uri
     * @param context
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun retornaPathImage(uri: Uri, context: Context): String? {
        // check here to KITKAT or new version
        val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                if ("primary".equals(type, ignoreCase = true)) {
                    return (context.getExternalFilesDir(null).toString() + "/"
                            + split[1])
                }
            } else if (isDownloadsDocument(uri)) {

                val id = DocumentsContract.getDocumentId(uri)
                val contentUri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"),
                    java.lang.Long.valueOf(id)
                )

                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                var contentUri: Uri? = null
                if ("image" == type) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }

                val selection = "_id=?"
                val selectionArgs = arrayOf(split[1])

                return getDataColumn(
                    context, contentUri, selection,
                    selectionArgs
                )
            }// MediaProvider
            // DownloadsProvider
        } else if ("content".equals(uri.scheme!!, ignoreCase = true)) {

            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
                context,
                uri,
                null,
                null
            )

        } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }// File
        // MediaStore (and general)

        return null
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    fun getDataColumn(
        context: Context, uri: Uri?,
        selection: String?, selectionArgs: Array<String>?
    ): String? {

        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)

        try {
            cursor = context.contentResolver.query(
                uri!!, projection,
                selection, selectionArgs, null
            )
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri
            .authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri
            .authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri
            .authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri
            .authority
    }

    /** SCROLLING BUTTON **/
    fun mScrolled(
        view: FloatingActionButton,
        context: Context,
        ativa_desativa: Boolean
    ): RecyclerView.OnScrollListener {
        return object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                try {
                    if (ativa_desativa) {
                        if (dy > 0 && view.visibility == View.VISIBLE)
                            view.hide()
                        else if (dy < 0 && view.visibility != View.VISIBLE)
                            view.show()
                    }

                } catch (err: java.lang.Exception) {
                    LogTrace.logCatch(context, this.javaClass, err, false)
                }

            }
        }
    }

    @Throws(Exception::class)
    fun validaDuasDatas_2(dataInicio: String, dataFim: String): Boolean {

        var resultado = true
        val dataInicial = SimpleDateFormat("dd/MM/yyyy").parse(dataInicio)
        val dataFinal = SimpleDateFormat("dd/MM/yyyy").parse(dataFim)

        if (dataFinal.before(dataInicial))
            resultado = false

        return resultado
    }


    @Throws(Exception::class)
    fun validaDataDias(data_1: String, dias: Int): Boolean {

        var resultado = true

        val data_2 = FormataData.retornaDataFormat(FormataData.DiaMesAno)

        val data_digitada = SimpleDateFormat("dd/MM/yyyy").parse(data_1)
        val data_hoje = SimpleDateFormat("dd/MM/yyyy").parse(data_2)

        val total_dias = (data_digitada.time - data_hoje.time) / 1000 / 60 / 60 / 24
        //val total_dias = TimeUnit.MICROSECONDS.toDays(mil)

        if (total_dias > dias)
            resultado = false

        return resultado
    }

    @Throws(Exception::class)
    fun checkAppIsRunning(context: Context): Boolean {
        var retorno = false
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val lista = manager.runningAppProcesses

        if (!lista.isNullOrEmpty()) {
            for (process in lista)
                if (process.processName == "br.com.spotpromo.havaianas")
                    retorno = true
        }

        return retorno
    }

    @Throws(Exception::class)
    fun ToastMessage(texto: String, context: Context) {
        val toast = Toast(context)

        toast.apply {
            val layout = LayoutInflater.from(context).inflate(R.layout.layout_toast, null)
            layout.texto.text = texto
            setGravity(Gravity.TOP, 0, 0)
            duration = Toast.LENGTH_LONG
            view = layout
            show()
        }
    }

    @Throws(java.lang.Exception::class)
    fun retornaCaminhoFotoURINew(
        uriAtual: Uri?,
        codRoteiro: Int,
        nomefoto: String?,
        context: Context
    ): String? {
        var retorna_novo_caminho_ou_antigo = ""
        val sdcard = context.getExternalFilesDir(null)

        /** PASTA PROJETO  */
        val pasta_projeto = File(
            sdcard!!.absolutePath + File.separator + SpotUtilConfig.caminho_db
        )
        if (!pasta_projeto.isDirectory) pasta_projeto.mkdirs()
        /** PASTA SPOT FOTOS  */
        val pasta_spot = File(
            pasta_projeto.absolutePath + File.separator + SpotUtilConfig.caminho_fotos
        )
        if (!pasta_spot.isDirectory) pasta_spot.mkdirs()
        val pasta_roteiro =
            File(pasta_spot.absolutePath + File.separator + codRoteiro)
        if (!pasta_roteiro.isDirectory) pasta_roteiro.mkdirs()
        val stringi_file_copy: String = CriarFileTemporarioStringRoteiro(
            pasta_roteiro.absolutePath,
            nomefoto!!,
            codRoteiro
        )
        val file_copy = File(stringi_file_copy)

        /** COPIAR ARQUIVO PARA NOVA PASTA  */
        var `in`: InputStream? = null
        var out: OutputStream? = null
        try {
            `in` = context.contentResolver.openInputStream(uriAtual!!)
            out = FileOutputStream(file_copy)

            val buf = ByteArray(1024)
            var len: Int
            while (`in`!!.read(buf).also { len = it } > 0) {
                out.write(buf, 0, len)
            }

            if (file_copy.isFile) retorna_novo_caminho_ou_antigo = file_copy.absolutePath
        } finally {
            `in`?.close()
            out?.close()
        }

        val file_movido = File(retorna_novo_caminho_ou_antigo)
        if (!file_movido.isFile)
            throw Exception("A foto não foi atualizada, por favor tente novamente")

        return retorna_novo_caminho_ou_antigo

    }

    @Throws(java.lang.Exception::class)
    fun CriarFileTemporarioStringRoteiro(
        diretorio: String,
        nomefoto: String,
        codRoteiro: Int
    ): String {
        val timeStamp =
            SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(Date())
        val mediaFile: File
        mediaFile =
            File(diretorio + File.separator + nomefoto + "_" + codRoteiro + "_" + timeStamp + ".jpg")
        return mediaFile.absolutePath
    }
}