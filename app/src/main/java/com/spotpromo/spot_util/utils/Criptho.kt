package com.spotpromo.spot_util.utils

import android.util.Base64
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec


class Criptho(key: ByteArray, iv: ByteArray)  {

    private var mKey = byteArrayOf()
    private var mIV = byteArrayOf()

    init {
        this.mKey = key
        this.mIV = iv
    }

    @Throws(Exception::class)
    fun encode(value: String, mode: Int): String {
        val cipher: Cipher
        var bytes: ByteArray

        cipher = this.getCipher(Cipher.ENCRYPT_MODE)
        bytes = cipher.doFinal(value.toByteArray())

        if (mode == BASE64_MODE)
            bytes = Base64.encodeToString(bytes, Base64.DEFAULT).toByteArray()

        return String(bytes, 0, bytes.size)
    }

    @Throws(Exception::class)
    fun decode(value: String, mode: Int): String {
        val cipher: Cipher
        val bytes: ByteArray

        cipher = this.getCipher(Cipher.DECRYPT_MODE)

        if (mode == BASE64_MODE)
            bytes = cipher.doFinal(Base64.decode(value.toByteArray(), Base64.DEFAULT))
        else
            bytes = cipher.doFinal(value.toByteArray())

        return String(bytes, 0, bytes.size)
    }

    @Throws(Exception::class)
    private fun getCipher(mode: Int): Cipher {

        val skeySpec = SecretKeySpec(mKey, "AES")
        val ivSpec = IvParameterSpec(mIV)

        val cipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
        cipher.init(mode, skeySpec, ivSpec)
        return cipher
    }

    companion object {

        val NORMAL_MODE = 0
        val BASE64_MODE = 1
    }
}
