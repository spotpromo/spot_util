package com.spotpromo.spot_util.utils

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import java.text.NumberFormat
import java.util.*

class TextWatcherFormat(private val context: Context, private val editText: EditText) : TextWatcher {

    private var formated: String? = null
    private var current: String? = ""
    private val meuLocal = Locale("pt", "BR")

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        try {

            if (s.toString() != current) {
                editText.removeTextChangedListener(this)

                val cleanString = s.toString().replace(("[R]").toRegex(), "").replace(("[$]").toRegex(), "").replace(("[,]").toRegex(), "").replace(("[.]").toRegex(), "");
                val parsed = java.lang.Double.parseDouble(cleanString)
                formated = NumberFormat.getCurrencyInstance(meuLocal).format(parsed / 100)
                current = formated
                formated = formated.toString().replace("\u00A0", "")

                editText.setText(formated.toString().replace("\u00A0", ""))
                editText.setSelection(formated!!.length)

                editText.addTextChangedListener(this)
            }

        } catch (e: Exception) {
            LogTrace.logCatch(context, this.javaClass, e, true)
        }

    }

    override fun afterTextChanged(s: Editable) {

    }
}