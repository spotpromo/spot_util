package com.spotpromo.spot_util.utils

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import java.text.NumberFormat
import java.util.*


class TextWatcherFormaterFormulario(
    private val context: Context,
    private val editText: EditText,
    private val type: String
) :
    TextWatcher {

    lateinit var formated: String
    private var current = ""
    private val maskCPF = "###.###.###-##"
    private val maskCNPJ = "##.###.###/####-##"
    private val maskData = "##/##/####"
    private val meuLocal = Locale("pt", "BR")
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        try {

            if (s.toString() != current) {
                editText.removeTextChangedListener(this)

                if (EnumTipo.EnumTipoText.valueOf(type) === EnumTipo.EnumTipoText.CPF) {
                    formated = Mask(maskCPF).format(s.toString())
                    current = formated
                } else if (EnumTipo.EnumTipoText.valueOf(type) === EnumTipo.EnumTipoText.CNPJ) {
                    formated = Mask(maskCNPJ).format(s.toString())
                    current = formated
                } else if (EnumTipo.EnumTipoText.valueOf(type) === EnumTipo.EnumTipoText.DATA) {
                    formated = Mask(maskData).format(s.toString())
                    current = formated
                } else if (EnumTipo.EnumTipoText.valueOf(type) === EnumTipo.EnumTipoText.PRECO) {
                    val cleanString = s.toString().replace(("[R]").toRegex(), "").replace(("[$]").toRegex(), "").replace(("[,]").toRegex(), "").replace(("[.]").toRegex(), "");
                    val parsed = java.lang.Double.parseDouble(cleanString)
                    formated = NumberFormat.getCurrencyInstance(meuLocal).format(parsed / 100)
                    current = formated
                    formated = formated.replace("\u00A0", "")
                } else if (EnumTipo.EnumTipoText.valueOf(type) === EnumTipo.EnumTipoText.NUMERIC) {
                    formated = s.toString()
                    current = formated
                } else if (EnumTipo.EnumTipoText.valueOf(type) === EnumTipo.EnumTipoText.PHONE) {
                    formated = s.toString()
                    current = formated
                } else if (EnumTipo.EnumTipoText.valueOf(type) === EnumTipo.EnumTipoText.NORMAL) {
                    formated = s.toString()
                    current = formated
                }

                editText.setText(formated.replace("\u00A0", ""))
                editText.setSelection(formated.length)

                editText.addTextChangedListener(this)
            }

        } catch (e: Exception) {
            LogTrace.logCatch(context, this.javaClass, e, true)
        }

    }

    override fun afterTextChanged(s: Editable) {

    }

}