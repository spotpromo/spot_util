package com.spotpromo.spot_util.utils

import android.app.DatePickerDialog
import android.content.Context
import android.view.View
import android.widget.EditText
import com.spotpromo.spot_util.R
import java.util.*


class DataDialogTabloide(
    private val edt: EditText,
    private val context: Context
) : View.OnClickListener {

    override fun onClick(v: View) {

        edt.isClickable = false
        val mcurrentDate = Calendar.getInstance()
        val mYear = mcurrentDate.get(Calendar.YEAR)
        val mMonth = mcurrentDate.get(Calendar.MONTH)
        val mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH)

        val mDatePicker = DatePickerDialog(context,
            DatePickerDialog.OnDateSetListener { datepicker, selectedyear, selectedmonth, selectedday ->
                edt.setText("")
                var dia = "0$selectedday"
                var mes = "0" + (selectedmonth + 1).toString()
                dia = dia.substring(dia.length - 2, dia.length)
                mes = mes.substring(mes.length - 2, mes.length)
                val data = "$dia/$mes/$selectedyear"
                edt.setText(data)

            }, mYear, mMonth, mDay
        )
        mDatePicker.setTitle(context.resources.getString(R.string.selecione_data))
        val dataMax = Date()
        val dataMin = Date()

        dataMax.time = dataMax.time + 15L * 24 * 60 * 60 * 1000
        dataMin.time = dataMin.time - 7L * 24 * 60 * 60 * 1000

        val c15Dias = Calendar.getInstance()
        val c7Dias = Calendar.getInstance()
        c15Dias.time = dataMax
        c7Dias.time = dataMin

        mDatePicker.datePicker.minDate = c7Dias.timeInMillis
        mDatePicker.datePicker.maxDate = c15Dias.timeInMillis
        mDatePicker.show()
    }
}