package com.spotpromo.spot_util.utils


/**
 * Classe que cria mascara
 */
class Mask(private val mMask: String) {
    internal var isUpdating: Boolean = false
    internal var old = ""

    /**
     * Retorna texto com a mascara informada
     *
     * @param texto
     * @return
     */
    fun format(texto: String): String {
        val str = unmask(texto)
        var mascara = ""
        if (isUpdating) {
            old = str
            isUpdating = false
            return texto
        }
        var i = 0
        for (m in mMask.toCharArray()) {

            if (m != '#' && str.length > old.length) {
                mascara += m
                continue
            }
            try {
                mascara += str[i]

            } catch (e: Exception) {
                break
            }

            i++
        }
        isUpdating = true

        /*String t = mascara.substring(mascara.length() - 1, mascara.length());
        if (t.indexOf("-") != 0 || t.indexOf(".") != 0 || t.indexOf("/") != 0 || t.indexOf("(") != 0 || t.indexOf(")") != 0) {
            mascara = mascara.substring(0, mascara.length() - 1);
        }*/
        return mascara
    }

    companion object {

        //Tira caracteres especiais
        fun unmask(s: String): String {
            return s.replace("[.]".toRegex(), "").replace("[-]".toRegex(), "")
                .replace("[/]".toRegex(), "").replace("[(]".toRegex(), "")
                .replace("[)]".toRegex(), "")
        }
    }
}