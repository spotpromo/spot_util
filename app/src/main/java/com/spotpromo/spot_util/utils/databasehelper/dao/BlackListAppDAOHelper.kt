package com.spotpromo.spot_util.utils.databasehelper.dao

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.spotpromo.spot_util.R
import com.spotpromo.spot_util.utils.Util
import com.spotpromo.spot_util.utils.databasehelper.DAOHelper
import com.spotpromo.spot_util.utils.model.BlackListApp
import com.spotpromo.spot_util.utils.model.BlackListAppNew
import java.util.ArrayList

class BlackListAppDAOHelper @Throws(Exception::class)
constructor(db: SQLiteDatabase) : DAOHelper(db) {

    /**
     * METODO INSERIR
     *
     * @param blackListApp
     * @throws Exception
     */
    @Throws(Exception::class)
    fun insert(blackListApp: BlackListApp) {
        val sbQuery = StringBuilder()
        sbQuery.append("INSERT INTO TB_BlackList_App (nome, packages) " + "VALUES(?,?)")

        this.executaNoQuery(
            sbQuery.toString(),
            blackListApp.nome!!,
            blackListApp.packages!!
        )
    }

    @Throws(Exception::class)
    fun insert(blackListApp: BlackListAppNew) {
        val sbQuery = StringBuilder()
        sbQuery.append("INSERT INTO TB_BlackList_App (nome, packages) " + "VALUES(?,?)")

        this.executaNoQuery(
            sbQuery.toString(),
            blackListApp.nome!!,
            blackListApp.packages!!
        )
    }

    @Throws(Exception::class)
    fun deletar() {
        val sbQuery = StringBuilder()
        sbQuery.append("DELETE FROM TB_BlackList_App")

        this.executaNoQuery(
            sbQuery.toString()
        )
    }

    /**
     * INSERT LISTA
     * @param lista
     * @param context
     * @throws Exception
     */
    @Throws(Exception::class)
    fun inserir(lista: ArrayList<BlackListApp>, context: Context) {
        Util.manipulaLoading(context, lista.size, "", 3)
        for (i in lista.indices) {
            insert(lista[i])
            Util.manipulaLoading(
                context,
                0,
                String.format(
                    context.resources.getString(R.string.loading_manipulacao).replace(
                        "_TIPO",
                        context.resources.getString(R.string.msg_insert_dados_configuracao)
                    ), i, lista.size
                ),
                1
            )
            Util.manipulaLoading(context, i, "", 2)
        }
    }

    @Throws(Exception::class)
    fun inserirNew(lista: ArrayList<BlackListAppNew>) {
        for (i in lista.indices) {
            insert(lista[i])
        }
    }

    /**
     * SELECT
     * @return
     */
    @Throws(Exception::class)
    fun select(): ArrayList<BlackListApp> {
        var cursor: Cursor? = null

        val sbQuery = StringBuilder()

        sbQuery.append("SELECT * FROM TB_BlackList_App")

        cursor = this.executaQuery(sbQuery.toString())
        val mLista = ArrayList<BlackListApp>()
        if (cursor != null && cursor.moveToFirst()) {
            do {
                val blackListApp = BlackListApp()
                blackListApp.nome = cursor.getString(cursor.getColumnIndex("nome"))
                blackListApp.packages = cursor.getString(cursor.getColumnIndex("packages"))
                mLista.add(blackListApp)
            } while (cursor.moveToNext())
        }

        cursor?.close()

        return mLista
    }
}