package com.spotpromo.spot_util.utils.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class TipoFoto : Serializable {
    @SerializedName("codTipoExecucao")
    var codTipoExecucao: Int? = null
    @SerializedName("desTipoExecucao")
    var desTipoExecucao: String? = null
    @SerializedName("qtdFotoMin")
    var qtdFotoMin: Int? = null
    @SerializedName("qtdFotoMax")
    var qtdFotoMax: Int? = null
    @SerializedName("codTipoFoto")
    var codTipoFoto: Int? = null
    @SerializedName("desTipoFoto")
    var desTipoFoto: String? = null
    @SerializedName("flObrigatorio")
    var flObrigatorio: Int? = null
}