package com.spotpromo.spot_util.utils

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import java.text.DecimalFormat

class TextWatcherFormatPercentual(private val context: Context, private val editText: EditText) : TextWatcher {

    private var formated: String? = null
    private var current: String? = ""

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        try {

            if(s.isNotEmpty() && s != current) {
                editText.removeTextChangedListener(this)

                var limpar_string = s.toString().replace("[%,.]".toRegex(), "")
                var parsed = limpar_string.toDouble()
                val decimalFormat = DecimalFormat("0.00")

                formated = decimalFormat.format(parsed/100)

                current = formated
                formated = formated.toString().replace(",", ".").replace("\u00A0", "")
                editText.setText(formated)
                editText.setSelection(formated.toString().length)

                editText.addTextChangedListener(this)
            }

        } catch (e: Exception) {
            LogTrace.logCatch(context, this.javaClass, e, true)
        }

    }

    override fun afterTextChanged(s: Editable) {

    }
}