package com.spotpromo.spot_util.utils.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class BlackListApp : Serializable {

    @SerializedName("nomeApp")
    var nome: String? = null
    @SerializedName("packge")
    var packages: String? = null
}